# Presentations

Here you can find all presentations off Golang Rotterdam Meetups.

If you want to hear more please join us on channel #rotterdam at the existing gophers.slack.com workspace. 
Sign up through [Gopher Slack](https://invite.slack.golangbridge.org/).
And go to the Rotterdam channel.

Cheers,

Go Rotterdam!!!